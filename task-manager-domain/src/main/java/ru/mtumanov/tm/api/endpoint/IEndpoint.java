package ru.mtumanov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;

import javax.jws.WebMethod;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.MalformedURLException;
import java.net.URL;

public interface IEndpoint {

    @NotNull
    String HOST = "localhost";

    @NotNull
    String PORT = "6060";

    @NotNull
    String REQUEST = "request";

    @NotNull
    String SPACE = "http://endpoint.tm.mtumanov.ru/";

    @WebMethod(exclude = true)
    static <T> T newInstance(
            @NotNull final String host,
            @NotNull final String port,
            @NotNull final String name,
            @NotNull final String space,
            @NotNull final String part,
            @NotNull final Class<T> clazz
    ) throws MalformedURLException {
        @NotNull final String wsdl = "http://" + host + ":" + port + "/" + name + "?wsdl";
        @NotNull final URL url = new URL(wsdl);
        @NotNull final QName qName = new QName(space, part);
        return Service.create(url, qName).getPort(clazz);
    }

    @WebMethod(exclude = true)
    static <T> T newInstance(
            @NotNull final String name,
            @NotNull final String part,
            @NotNull final Class<T> clazz
    ) throws MalformedURLException {
        @NotNull final String host = HOST;
        @NotNull final String port = PORT;
        @NotNull final String space = SPACE;
        return newInstance(host, port, name, space, part, clazz);
    }

    @WebMethod(exclude = true)
    static <T> T newInstance(
            @NotNull final String host,
            @NotNull final String port,
            @NotNull final String name,
            @NotNull final String part,
            @NotNull final Class<T> clazz
    ) throws MalformedURLException {
        @NotNull final String space = SPACE;
        return newInstance(host, port, name, space, part, clazz);
    }

}
