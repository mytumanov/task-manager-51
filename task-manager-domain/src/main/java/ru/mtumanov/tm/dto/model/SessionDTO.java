package ru.mtumanov.tm.dto.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.enumerated.Role;

import javax.persistence.*;
import java.util.Date;

@Entity
@Getter
@Setter
@Table(name = "tm_session")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class SessionDTO extends AbstractUserOwnedModelDTO {

    @NotNull
    @Column(nullable = false)
    private Date created = new Date();

    @Nullable
    @Column
    @Enumerated(EnumType.STRING)
    private Role role = null;

}
