package ru.mtumanov.tm.exception.field;

public final class DescriptionEmptyException extends AbstractFieldException {

    public DescriptionEmptyException() {
        super("ERROR! Description is empty!");
    }

}
