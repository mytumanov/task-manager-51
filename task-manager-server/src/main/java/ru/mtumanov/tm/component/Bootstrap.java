package ru.mtumanov.tm.component;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.api.endpoint.*;
import ru.mtumanov.tm.api.service.*;
import ru.mtumanov.tm.api.service.dto.*;
import ru.mtumanov.tm.endpoint.*;
import ru.mtumanov.tm.enumerated.Role;
import ru.mtumanov.tm.service.*;
import ru.mtumanov.tm.service.dto.*;
import ru.mtumanov.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public final class Bootstrap implements IServiceLocator {

    @NotNull
    @Getter
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    @Getter
    private final IDtoProjectTaskService projectTaskService = new ProjectTaskDtoService(connectionService);

    @NotNull
    @Getter
    private final IDtoProjectService projectService = new ProjectDtoService(connectionService);

    @NotNull
    @Getter
    private final IDtoTaskService taskService = new TaskDtoService(connectionService);

    @NotNull
    @Getter
    private final ILoggerService loggerService = new LoggerService(connectionService);

    @NotNull
    private final IDtoSessionService sessionService = new SessionDtoService(connectionService);

    @NotNull
    @Getter
    private final IDtoUserService userService = new UserDtoService(connectionService, propertyService);

    @NotNull
    @Getter
    private final IAuthService authService = new AuthService(userService, propertyService, sessionService);

    @NotNull
    @Getter
    private final IDomainService domainService = new DomainService(this);

    @NotNull
    private final Backup backup = new Backup(this);

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final IAuthEndpoint authEndpoint = new AuthEndpoint(this);

    {
        registry(systemEndpoint);
        registry(domainEndpoint);
        registry(projectEndpoint);
        registry(taskEndpoint);
        registry(userEndpoint);
        registry(authEndpoint);
    }

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final Integer port = propertyService.getServerPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        loggerService.debug("url: " + url);
        Endpoint.publish(url, endpoint);
    }

    private void initPID() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        try {
            Files.write(Paths.get(fileName), pid.getBytes());
        } catch (@NotNull final IOException e) {
            loggerService.error(e);
        }
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    public void start() {
        loggerService.info("** WELCOME TO TASK_MANAGER **");
        loggerService.initJmsLogger();
        Runtime.getRuntime().addShutdownHook(new Thread(this::stop));
        initDemo();
        initPID();
    }

    private void initDemo() {
        try {
            userService.create("COOL_USER", "cool", Role.ADMIN);
            userService.create("NOT_COOL_USER", "Not Cool", "notcool@email.ru");
            userService.create("123qwe!", "vfda4432!", "vfda4432@email.ru");
        } catch (final Exception e) {
            System.out.println("ERROR! Fail to initialize test data.");
            System.out.println(e.getMessage());
        }
    }

    private void stop() {
        loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
        backup.stop();
    }

}
