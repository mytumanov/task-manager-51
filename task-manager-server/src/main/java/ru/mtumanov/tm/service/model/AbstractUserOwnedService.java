package ru.mtumanov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.api.repository.model.IUserOwnedRepository;
import ru.mtumanov.tm.api.repository.model.IUserRepository;
import ru.mtumanov.tm.api.service.IConnectionService;
import ru.mtumanov.tm.api.service.model.IUserOwnedService;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.exception.user.UserIdEmptyException;
import ru.mtumanov.tm.model.AbstractUserOwnedModel;
import ru.mtumanov.tm.model.User;
import ru.mtumanov.tm.repository.model.UserRepository;

import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel, R extends IUserOwnedRepository<M>>
        extends AbstractService<M, R> implements IUserOwnedService<M> {

    @NotNull
    private final IUserRepository userRepository;

    protected AbstractUserOwnedService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
        this.userRepository = new UserRepository(connectionService);
    }

    @Override
    @NotNull
    protected abstract R getRepository();

    @NotNull
    protected IUserRepository getUserRepository() {
        return userRepository;
    }

    @Override
    @NotNull
    public M add(@NotNull final String userId, @NotNull final M model) throws AbstractException {
        if (userId.isEmpty())
            throw new UserIdEmptyException();
        @NotNull final User user = getUserRepository().findOneById(userId);
        model.setUser(user);
        repository.add(model);
        return model;
    }

    @Override
    public void clear(@NotNull final String userId) {
        repository.clear(userId);
    }

    @Override
    public boolean existById(@NotNull final String userId, @NotNull final String id) {
        return repository.existById(userId, id);
    }

    @Override
    @NotNull
    public List<M> findAll(@NotNull final String userId) {
        return repository.findAll(userId);
    }

    @Override
    @NotNull
    public List<M> findAll(@NotNull final String userId, @Nullable final Comparator<M> comparator) {
        if (comparator == null)
            return repository.findAll(userId);
        return repository.findAll(userId, comparator);
    }

    @Override
    @NotNull
    public M findOneById(@NotNull final String userId, @NotNull final String id) {
        return repository.findOneById(userId, id);
    }

    @Override
    public long getSize(@NotNull final String userId) {
        return repository.getSize(userId);
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final M model) {
        repository.remove(model);
    }

    @Override
    public void removeById(@NotNull final String userId, @NotNull final String id) {
        repository.removeById(userId, id);
    }

}
