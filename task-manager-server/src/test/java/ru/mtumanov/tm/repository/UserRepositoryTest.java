package ru.mtumanov.tm.repository;


import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.mtumanov.tm.api.repository.model.IProjectRepository;
import ru.mtumanov.tm.api.repository.model.ITaskRepository;
import ru.mtumanov.tm.api.repository.model.IUserRepository;
import ru.mtumanov.tm.api.service.IConnectionService;
import ru.mtumanov.tm.api.service.IPropertyService;
import ru.mtumanov.tm.enumerated.Role;
import ru.mtumanov.tm.marker.DBCategory;
import ru.mtumanov.tm.migration.AbstractSchemeTest;
import ru.mtumanov.tm.model.Project;
import ru.mtumanov.tm.model.Task;
import ru.mtumanov.tm.model.User;
import ru.mtumanov.tm.repository.model.ProjectRepository;
import ru.mtumanov.tm.repository.model.TaskRepository;
import ru.mtumanov.tm.repository.model.UserRepository;
import ru.mtumanov.tm.service.ConnectionService;
import ru.mtumanov.tm.service.PropertyService;

import javax.persistence.NoResultException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@Category(DBCategory.class)
public class UserRepositoryTest extends AbstractSchemeTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final IUserRepository userRepository = new UserRepository(connectionService);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository(connectionService);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository(connectionService);

    @NotNull
    private final List<User> userList = new ArrayList<>();

    @BeforeClass
    public static void init() throws LiquibaseException {
        @NotNull final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");
    }

    @Before
    public void initRepository() throws Exception {
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final User user = new User();
            user.setFirstName("UserFirstName " + i);
            user.setLastName("UserLastName " + i);
            user.setMiddleName("UserMidName " + i);
            user.setEmail("user" + i + "@dot.ru");
            user.setLogin("USER" + i);
            user.setRole(Role.USUAL);
            user.setPasswordHash("123" + i);
            userRepository.add(user);
            userList.add(user);

            for (int j = 0; j < NUMBER_OF_ENTRIES; j++) {
                @NotNull final Project project = new Project();
                project.setName("ProjectDTO name: " + j);
                project.setDescription("ProjectDTO description: " + j);
                project.setUser(user);
                projectRepository.add(project);

                for (int a = 0; a < NUMBER_OF_ENTRIES; a++) {
                    @NotNull final Task task = new Task();
                    task.setName("TaskDTO name: " + a);
                    task.setDescription("TaskDTO description: " + a);
                    task.setUser(user);
                    task.setProject(project);
                    taskRepository.add(task);
                }
            }
        }
    }

    @After
    public void clearRepository() throws Exception {
        userRepository.clear();
        userList.clear();
    }

    @Test
    public void testFindByEmail() throws Exception {
        for (@NotNull final User user : userList) {
            assertEquals(user, userRepository.findByEmail(user.getEmail()));
        }
    }

    @Test(expected = NoResultException.class)
    public void testExceptionFindByEmail() throws Exception {
        userRepository.findByEmail("notvalidemail@dot.ru");
    }

    @Test
    public void testFindByLogin() throws Exception {
        for (@NotNull final User user : userList) {
            assertEquals(user, userRepository.findByLogin(user.getLogin()));
        }
    }

    @Test(expected = NoResultException.class)
    public void testExceptionFindByLogin() throws Exception {
        userRepository.findByLogin("NOTVALIDLOGIN");
    }

    @Test
    public void testIsEmailExist() throws Exception {
        for (@NotNull final User user : userList) {
            assertTrue(userRepository.isEmailExist(user.getEmail()));
            assertFalse(userRepository.isEmailExist(user.getEmail() + user.getEmail()));
        }
    }

    @Test
    public void testIsLoginExist() throws Exception {
        for (@NotNull final User user : userList) {
            assertTrue(userRepository.isLoginExist(user.getLogin()));
            assertFalse(userRepository.isEmailExist(user.getLogin() + user.getLogin()));
        }
    }

    @Test
    public void testRemoveById() throws Exception {
        for (@NotNull final User user : userList) {
            userRepository.removeById(user.getId());
            assertFalse(userRepository.existById(user.getId()));
        }
    }
}
