package ru.mtumanov.tm.service;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.mtumanov.tm.api.repository.dto.IDtoProjectRepository;
import ru.mtumanov.tm.api.repository.dto.IDtoTaskRepository;
import ru.mtumanov.tm.api.repository.dto.IDtoUserRepository;
import ru.mtumanov.tm.api.service.IConnectionService;
import ru.mtumanov.tm.api.service.IPropertyService;
import ru.mtumanov.tm.api.service.dto.IDtoUserService;
import ru.mtumanov.tm.dto.model.ProjectDTO;
import ru.mtumanov.tm.dto.model.TaskDTO;
import ru.mtumanov.tm.dto.model.UserDTO;
import ru.mtumanov.tm.enumerated.Role;
import ru.mtumanov.tm.exception.field.EmailEmptyException;
import ru.mtumanov.tm.exception.field.IdEmptyException;
import ru.mtumanov.tm.exception.field.LoginEmptyException;
import ru.mtumanov.tm.exception.field.PasswordEmptyException;
import ru.mtumanov.tm.marker.DBCategory;
import ru.mtumanov.tm.migration.AbstractSchemeTest;
import ru.mtumanov.tm.repository.dto.ProjectDtoRepository;
import ru.mtumanov.tm.repository.dto.TaskDtoRepository;
import ru.mtumanov.tm.repository.dto.UserDtoRepository;
import ru.mtumanov.tm.service.dto.UserDtoService;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@Category(DBCategory.class)
public class UserServiceTest extends AbstractSchemeTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static final IDtoUserService userService = new UserDtoService(connectionService, propertyService);

    @NotNull
    private static final List<UserDTO> userList = new ArrayList<>();

    @BeforeClass
    public static void init() throws LiquibaseException {
        @NotNull final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");
    }

    @Before
    public void initRepository() throws Exception {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IDtoProjectRepository projectRepository = new ProjectDtoRepository(entityManager);
        @NotNull final IDtoTaskRepository taskRepository = new TaskDtoRepository(entityManager);
        @NotNull final IDtoUserRepository userRepository = new UserDtoRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
                @NotNull final UserDTO user = new UserDTO();
                user.setFirstName("UserFirstName " + i);
                user.setLastName("UserLastName " + i);
                user.setMiddleName("UserMidName " + i);
                user.setEmail("user" + i + "@dot.ru");
                user.setLogin("USER" + i);
                user.setRole(Role.USUAL);
                user.setPasswordHash("123" + i);
                userRepository.add(user);
                userList.add(user);

                for (int j = 0; j < NUMBER_OF_ENTRIES; j++) {
                    @NotNull final ProjectDTO project = new ProjectDTO();
                    project.setName("ProjectDTO name: " + j);
                    project.setDescription("ProjectDTO description: " + j);
                    project.setUserId(user.getId());
                    projectRepository.add(project);

                    for (int a = 0; a < NUMBER_OF_ENTRIES; a++) {
                        @NotNull final TaskDTO task = new TaskDTO();
                        task.setName("TaskDTO name: " + a);
                        task.setDescription("TaskDTO description: " + a);
                        task.setUserId(user.getId());
                        task.setProjectId(project.getId());
                        taskRepository.add(task);
                    }
                }
            }
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @After
    public void clearRepository() throws Exception {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IDtoProjectRepository projectRepository = new ProjectDtoRepository(entityManager);
        @NotNull final IDtoTaskRepository taskRepository = new TaskDtoRepository(entityManager);
        @NotNull final IDtoUserRepository userRepository = new UserDtoRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            taskRepository.clear();
            projectRepository.clear();
            userRepository.clear();
            entityManager.getTransaction().commit();
            userList.clear();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void testCreate() throws Exception {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IDtoUserRepository userRepository = new UserDtoRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            @NotNull List<UserDTO> userListActual = userRepository.findAll();
            assertEquals(userList.size(), userListActual.size());

            userService.create("TEST_USER1", "TEST_PASSWORD");
            userListActual = userRepository.findAll();
            assertEquals(userList.size() + 1, userListActual.size());

            userService.create("TEST_USER2", "TEST_PASSWORD", "test@email.ru");
            userListActual = userRepository.findAll();
            assertEquals(userList.size() + 2, userListActual.size());

            userService.create("TEST_USER3", "TEST_PASSWORD", Role.USUAL);
            userListActual = userRepository.findAll();
            assertEquals(userList.size() + 3, userListActual.size());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }


    }

    @Test(expected = LoginEmptyException.class)
    public void testEmptyLoginCreate() throws Exception {
        userService.create("", "TEST_PASSWORD");
    }

    @Test(expected = PasswordEmptyException.class)
    public void testEmptyPswrdCreate() throws Exception {
        userService.create("LOGIN", "");
    }

    @Test(expected = LoginEmptyException.class)
    public void testEmptyLoginCreateWithEmail() throws Exception {
        userService.create("", "TEST_PASSWORD", "test@email.ru");
    }

    @Test(expected = PasswordEmptyException.class)
    public void testEmptyPswrdCreateWithEmail() throws Exception {
        userService.create("LOGIN", "", "test@email.ru");
    }

    @Test(expected = LoginEmptyException.class)
    public void testEmptyLoginCreateWithRole() throws Exception {
        userService.create("", "TEST_PASSWORD", Role.USUAL);
    }

    @Test(expected = PasswordEmptyException.class)
    public void testEmptyPswrdCreateWithRole() throws Exception {
        userService.create("LOGIN", "", Role.USUAL);
    }

    @Test
    public void testFindByEmail() throws Exception {
        for (@NotNull final UserDTO user : userList) {
            @NotNull final UserDTO actualUser = userService.findByEmail(user.getEmail());
            assertEquals(user, actualUser);
        }
    }

    @Test(expected = EmailEmptyException.class)
    public void testEmptyEmailFindByEmail() throws Exception {
        userService.findByEmail("");
    }

    @Test
    public void testFindByLogin() throws Exception {
        for (@NotNull final UserDTO user : userList) {
            @NotNull final UserDTO actualUser = userService.findByLogin(user.getLogin());
            assertEquals(user, actualUser);
        }
    }

    @Test(expected = LoginEmptyException.class)
    public void testEmptyLoginFindByLogin() throws Exception {
        userService.findByLogin("");
    }

    @Test
    public void testIsEmailExist() throws Exception {
        for (@NotNull final UserDTO user : userList) {
            assertTrue(userService.isEmailExist(user.getEmail()));
            assertFalse(userService.isEmailExist(user.getEmail() + user.getEmail()));
            assertFalse(userService.isEmailExist(""));
        }
    }

    @Test
    public void testIsLoginExist() throws Exception {
        for (@NotNull final UserDTO user : userList) {
            assertTrue(userService.isLoginExist(user.getLogin()));
            assertFalse(userService.isLoginExist(user.getLogin() + user.getLogin()));
            assertFalse(userService.isLoginExist(""));
        }
    }

    @Test
    public void testLockUserByLogin() throws Exception {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IDtoUserRepository userRepository = new UserDtoRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            for (@NotNull final UserDTO user : userList) {
                user.setLocked(false);
                assertFalse(user.isLocked());
                userService.lockUserByLogin(user.getLogin());
                @NotNull final UserDTO actualUser = userRepository.findOneById(user.getId());
                assertTrue(actualUser.isLocked());
            }
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test(expected = LoginEmptyException.class)
    public void testLoginEmptyLockUserByLogin() throws Exception {
        userService.lockUserByLogin("");
    }

    @Test
    public void testRemoveByEmail() throws Exception {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IDtoProjectRepository projectRepository = new ProjectDtoRepository(entityManager);
        @NotNull final IDtoTaskRepository taskRepository = new TaskDtoRepository(entityManager);
        @NotNull final IDtoUserRepository userRepository = new UserDtoRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            for (@NotNull final UserDTO user : userList) {
                assertNotNull(userRepository.findOneById(user.getId()));
                assertNotEquals(0, projectRepository.findAll(user.getId()).size());
                assertNotEquals(0, taskRepository.findAll(user.getId()).size());
                userService.removeByEmail(user.getEmail());

                entityManager.clear();
                assertNull(userRepository.findOneById(user.getId()));
                assertEquals(0, projectRepository.findAll(user.getId()).size());
                assertEquals(0, taskRepository.findAll(user.getId()).size());
            }
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void testRemoveByLogin() throws Exception {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IDtoProjectRepository projectRepository = new ProjectDtoRepository(entityManager);
        @NotNull final IDtoTaskRepository taskRepository = new TaskDtoRepository(entityManager);
        @NotNull final IDtoUserRepository userRepository = new UserDtoRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            for (@NotNull final UserDTO user : userList) {
                assertNotNull(userRepository.findOneById(user.getId()));
                assertNotEquals(0, projectRepository.findAll(user.getId()).size());
                assertNotEquals(0, taskRepository.findAll(user.getId()).size());
                userService.removeByLogin(user.getLogin());

                entityManager.clear();
                assertNull(userRepository.findOneById(user.getId()));
                assertEquals(0, projectRepository.findAll(user.getId()).size());
                assertEquals(0, taskRepository.findAll(user.getId()).size());
            }
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void testSetPassword() throws Exception {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IDtoUserRepository userRepository = new UserDtoRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            for (@NotNull final UserDTO user : userList) {
                @NotNull final String oldHash = user.getPasswordHash();
                userService.setPassword(user.getId(), "STRONPASS12@qq" + user.getLogin());
                @NotNull final UserDTO actualUser = userRepository.findOneById(user.getId());
                assertNotEquals(actualUser.getPasswordHash(), oldHash);
            }
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void testUnlockUserByLogin() throws Exception {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IDtoUserRepository userRepository = new UserDtoRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            for (@NotNull final UserDTO user : userList) {
                user.setLocked(true);
                assertTrue(user.isLocked());
                userService.unlockUserByLogin(user.getLogin());
                @NotNull final UserDTO actualUser = userRepository.findOneById(user.getId());
                assertFalse(actualUser.isLocked());
            }
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test(expected = LoginEmptyException.class)
    public void testLoginEmptyUnlockUserByLogin() throws Exception {
        userService.unlockUserByLogin("");
    }

    @Test
    public void testUserUpdate() throws Exception {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IDtoUserRepository userRepository = new UserDtoRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            for (@NotNull final UserDTO user : userList) {
                @NotNull final String firstName = user.getFirstName() + "TEST";
                @NotNull final String lastName = user.getLastName() + "TEST";
                @NotNull final String middleName = user.getMiddleName() + "TEST";
                assertNotEquals(user.getFirstName(), firstName);
                assertNotEquals(user.getLastName(), lastName);
                assertNotEquals(user.getMiddleName(), middleName);
                userService.userUpdate(user.getId(), firstName, lastName, middleName);
                @NotNull final UserDTO actualUser = userRepository.findOneById(user.getId());
                assertEquals(firstName, actualUser.getFirstName());
                assertEquals(lastName, actualUser.getLastName());
                assertEquals(middleName, actualUser.getMiddleName());
            }
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test(expected = IdEmptyException.class)
    public void testEmptyIdUserUpdate() throws Exception {
        userService.userUpdate("", "firstName", "lastName", "middleName");
    }
}
